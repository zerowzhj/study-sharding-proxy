package study.sharding.proxy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import study.sharding.proxy.mapper.userbase.UserBaseEO;
import study.sharding.proxy.mapper.userbase.UserBaseMapper;

@Service
public class UserService {

    @Autowired
    private UserBaseMapper userBaseMapper;

    public int add(UserBaseEO ubEO) {
        return userBaseMapper.insert(ubEO);
    }

    public UserBaseEO get(Long ubId) {
        return userBaseMapper.get(ubId);
    }
}
