package study.sharding.proxy.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import study.sharding.proxy.mapper.orderbase.OrderBaseEO;
import study.sharding.proxy.mapper.orderbase.OrderBaseMapper;
import study.sharding.proxy.mapper.orderitem.OrderItemEO;
import study.sharding.proxy.mapper.orderitem.OrderItemMapper;

@Slf4j
@Service
public class OrderService {

    @Autowired
    private OrderBaseMapper orderBaseMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;

    public int add(String orderNo) {
        OrderBaseEO rbEO = new OrderBaseEO();
        rbEO.setRbOrderNo(orderNo);
        int cnt = orderBaseMapper.insert(rbEO);

        OrderItemEO riEO = new OrderItemEO();
        riEO.setRiOrderNo(orderNo);
        orderItemMapper.insert(riEO);
        return cnt;
    }
}
