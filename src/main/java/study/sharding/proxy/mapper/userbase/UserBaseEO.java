package study.sharding.proxy.mapper.userbase;

import lombok.Data;
import org.apache.ibatis.type.Alias;
import study.sharding.proxy.mapper.BaseEO;

@Data
@Alias("UserBaseEO")
public class UserBaseEO extends BaseEO {

    private Long ubId;
    private String ubUserId;
    private String ubLoginName;
    private String ubLoginPwd;
}
