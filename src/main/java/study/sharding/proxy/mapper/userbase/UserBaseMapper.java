package study.sharding.proxy.mapper.userbase;

public interface UserBaseMapper {

    int insert(UserBaseEO ubEO);

    UserBaseEO get(Long ubId);
}
