package study.sharding.proxy.mapper.orderitem;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;
import study.sharding.proxy.mapper.BaseEO;

@Setter
@Getter
@ToString
@Alias("OrderItemEO")
public class OrderItemEO extends BaseEO {

    private Long riId;

    private String riOrderNo;
}
