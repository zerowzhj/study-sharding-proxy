package study.sharding.proxy.mapper.orderbase;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.type.Alias;
import study.sharding.proxy.mapper.BaseEO;

@Setter
@Getter
@ToString
@Alias("OrderBaseEO")
public class OrderBaseEO extends BaseEO {

    private Long rbId;

    private String rbOrderNo;
}
