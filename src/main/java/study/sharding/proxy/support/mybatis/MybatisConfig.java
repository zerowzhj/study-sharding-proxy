package study.sharding.proxy.support.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Component;

@MapperScan("study.sharding.proxy.mapper")
@Component
public class MybatisConfig {

}
