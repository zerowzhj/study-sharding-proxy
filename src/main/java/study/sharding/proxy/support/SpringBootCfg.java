package study.sharding.proxy.support;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "study.sharding.proxy")
public class SpringBootCfg {

}
