package test.study.sharding.shardingjdbc;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.sharding.proxy.mapper.userbase.UserBaseEO;
import study.sharding.proxy.mapper.userbase.UserBaseMapper;
import study.sharding.proxy.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootCfg.class})
public class MasterSlaveTest {

    @Autowired
    private UserBaseMapper userBaseMapper;

    @Test
    public void insert_test() {
        UserBaseEO ubEO = new UserBaseEO();

        ubEO.setUbUserId("12312");
        ubEO.setUbLoginName("1");
        ubEO.setUbLoginPwd("1");
        userBaseMapper.insert(ubEO);
    }

    @Test
    public void get_test() {
        UserBaseEO ubEO = userBaseMapper.get(1L);
        log.info("{}", ubEO);
    }
}
